package ru.coderiders.matcher;

import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public abstract class MatcherTest<T extends FileMatcher> {

    private T instance;

    protected abstract T createInstance();

    @Before
    public void setUp() {
        instance = createInstance();
    }

    @Test(expected = NullPointerException.class)
    public void testMatchNull(){
        File stub = null;
        this.instance.match(stub);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMatchExists(){
        File stub = mock(File.class);
        when(stub.exists()).thenReturn(false);
        this.instance.match(stub);
    }

    @Test
    public void testMatchIsDirectory(){
        File stub = mock(File.class);
        when(stub.exists()).thenReturn(true);
        when(stub.isDirectory()).thenReturn(true);
        assertEquals(false, this.instance.match(stub));
    }

    @Test
    public void testMatchExtensions(){
        File stub = mock(File.class);
        when(stub.exists()).thenReturn(true);
        when(stub.isDirectory()).thenReturn(false);
        String extension = this.instance.getExtensions().iterator().next();
        doReturn(String.format("test%s", extension)).when(stub).getName();
        assertEquals(true, this.instance.match(stub));
    }

}