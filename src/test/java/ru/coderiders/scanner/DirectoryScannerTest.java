package ru.coderiders.scanner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ru.coderiders.matcher.FileMatcher;
import ru.coderiders.matcher.PdfMatcher;
import ru.coderiders.matcher.PictureMatcher;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(value = Parameterized.class)
public class DirectoryScannerTest {

    private final FileMatcher matcher;
    private final boolean deepModeCheck;

    public DirectoryScannerTest(FileMatcher matcher, boolean deepModeCheck){
        this.matcher = matcher;
        this.deepModeCheck = deepModeCheck;
    }

    @Parameterized.Parameters(name = "{index}: Matcher - {0}, deepMode - {1}")
    public static List<Object[]> data() {
        var data = new Object[][] {
                { new PdfMatcher(), false },
                { new PdfMatcher(), true},
                { new PictureMatcher(), false},
                { new PictureMatcher(), true}};
        return Arrays.asList(data);
    }

    private DirectoryScanner makeScanner(Path directory){
        return new DirectoryScanner(directory, this.matcher, this.deepModeCheck);
    }

    private File makeFile(String name){
        File file = mock(File.class);
        doReturn(name).when(file).getName();
        when(file.isFile()).thenReturn(true);
        when(file.toPath()).thenReturn(mock(Path.class));
        when(file.exists()).thenReturn(true);
        return file;
    }

    private File makeDir(int limit){
        File dirStub = mock(File.class);
        when(dirStub.isFile()).thenReturn(false);
        ArrayList<File> fileList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            if (limit >= 1 & this.deepModeCheck) {
                var temp = makeDir(limit - 1);
                fileList.add(temp);
            }
            File fileStub = makeFile(String.format("test%s.pdf", i));
            fileList.add(fileStub);
            fileStub = makeFile(String.format("test%s.png", i));
            fileList.add(fileStub);
        }
        var tmp = fileList.toArray(new File[0]);
        doReturn(tmp).when(dirStub).listFiles();
        return dirStub;
    }

    @Test
    public void testScanNull(){
        var scan = makeScanner(null);
        assertThrows(IllegalStateException.class, scan::scan);
    }

    @Test
    public void testScanCorruptPath(){
        Path pathStub = mock(Path.class);
        when(pathStub.toFile()).thenThrow(UnsupportedOperationException.class);
        var scan = makeScanner(pathStub);
        assertThrows(IllegalStateException.class, scan::scan);
    }

    @Test
    public void testScanPathNotExists(){
        Path pathStub = mock(Path.class);
        File fileStub = mock(File.class);
        when(fileStub.exists()).thenReturn(false);
        doReturn(fileStub).when(pathStub).toFile();
        var scan = makeScanner(pathStub);
        assertThrows(IllegalArgumentException.class, scan::scan);
    }

    @Test
    public void testScanPathIsFile(){
        Path pathStub = mock(Path.class);
        File fileStub = mock(File.class);
        when(fileStub.exists()).thenReturn(true);
        when(fileStub.isFile()).thenReturn(true);
        doReturn(fileStub).when(pathStub).toFile();
        var scan = makeScanner(pathStub);
        assertThrows(IllegalArgumentException.class, scan::scan);
    }

    @Test
    public void testScanDirIsFile(){
        Path pathStub = mock(Path.class);
        File fileStub = makeFile("test.pdf");
        var scan = makeScanner(pathStub);
        if (this.matcher instanceof PdfMatcher) {
            assertEquals(1, scan.scanDir(fileStub).size());
        }
        if (this.matcher instanceof PictureMatcher) {
            assertEquals(0, scan.scanDir(fileStub).size());
        }
    }

    @Test
    public void testScanDirIsDirectory(){
        Path pathStub = mock(Path.class);
        File dirStub = makeDir(2);
        var scan = makeScanner(pathStub);
        assertEquals(this.deepModeCheck ? 84 : 4, scan.scanDir(dirStub).size());
    }

}
